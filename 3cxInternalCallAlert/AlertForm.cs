﻿namespace _3cxInternalCallAlert
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    public partial class AlertForm : Form
    {
        private readonly List<Color> colors;

        private readonly ColorFader fader = new ColorFader(Color.White, Color.Yellow, 10);

        private int timesToRepeat;

        private int tickCount;

        public AlertForm()
        {
            this.InitializeComponent();
            this.colors = this.fader.Fade().ToList();
            this.timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.BackColor = this.colors[this.tickCount];
            this.tickCount += 1;

            if (this.tickCount >= this.colors.Count)
            {
                if (this.timesToRepeat > Convert.ToInt32(ConfigurationManager.AppSettings["numberOfRepeats"]) - 1)
                {
                    this.timer1.Stop();
                    this.BackColor = Color.Gray;
                    this.Close();
                }
                this.colors.Reverse();
                this.tickCount = 0;
                this.timesToRepeat++;
            }
        }

        private void Anywhere_Click(object sender, EventArgs e)
        {
            this.timer1.Stop();
            this.Close();
        }
    }
}