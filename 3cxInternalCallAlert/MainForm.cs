﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3cxInternalCallAlert
{
    using System.Diagnostics;
    using System.Runtime.InteropServices;

    public partial class MainForm : Form
    {
        private List<AlertForm> alerts = new List<AlertForm>();
        public MainForm(string[] args)
        {
            InitializeComponent();
            if (args.Length < 2)
            {
                MessageBox.Show("two arguements must be passed, caller  number and caller name");
                Application.Exit();
            }

            if (
                shouldShowForAllCalls() || 
                this.checkIfInternalCall(args[0]) || 
                (shouldShowForAllSupportCalls() && checkIfSupportCall(args[1])))
            {
                this.ShowMsgOnAllScreens(args); 
            }
            else
            {
                Environment.Exit(0);
            }
        }

        bool shouldShowForAllCalls()
        {
            try
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["alertAllCalls"]);
            }
            catch
            {
                return false;
            }
        }

        bool shouldShowForAllSupportCalls()
        {
            try
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["alertAllSupportCalls"]);
            }
            catch
            {
                return false;
            }
        }

        bool checkIfSupportCall(string fromName)
        {
            return fromName.Contains("Support");
        }

        bool checkIfInternalCall(string fromNumber)
        {
            return !(fromNumber.StartsWith("0") || fromNumber.StartsWith("anonymous") || fromNumber.StartsWith("+"));
        }

        void ShowMsgOnAllScreens(string[] args)
        {
            foreach (Screen t in Screen.AllScreens)
            {
                AlertForm alert = new AlertForm();
                alert.label1.Text = "Call from " + args[0] + " - " + args[1];
                alert.StartPosition = FormStartPosition.Manual;
                alert.Bounds = t.Bounds;
                alert.Show();
                alert.Closed += Alert_Closed;
                alerts.Add(alert);
            }
        }

        private void Alert_Closed(object sender, EventArgs e)
        {
            CloseAllAlerts();
            Application.Exit();
        }

        void CloseAllAlerts()
        {
            foreach (var a in alerts)
            {
                a.Closed -= Alert_Closed;
                a.Close();
            }
        }

    }
}
